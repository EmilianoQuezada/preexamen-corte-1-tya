document.addEventListener("DOMContentLoaded", function () {
    const btnCargar = document.getElementById("btnCargar");
    const btnLimpiar = document.getElementById("btnLimpiar");
    const inputID = document.getElementById("inputID");
    const listaUsuarios = document.getElementById("lista");
    btnCargar.addEventListener("click", function () {
        const userId = inputID.value.trim();
        if (userId !== "") {
            hacerPeticion(`https://jsonplaceholder.typicode.com/users/${userId}`);
        } else {
            alert("Por favor, ingrese una ID para buscar.");
        }
    });
    btnLimpiar.addEventListener("click", function () {
        inputID.value = "";
        listaUsuarios.innerHTML = "";
    });
    function hacerPeticion(url) {
        listaUsuarios.innerHTML = "<tr><td colspan='7'>Cargando...</td></tr>";
        axios.get(url)
            .then(response => {
                const usuario = response.data;

                listaUsuarios.innerHTML = `<tr>
                    <td>${usuario.id}</td>
                    <td>${usuario.name}</td>
                    <td>${usuario.username}</td>
                    <td>${usuario.email}</td>
                    <td>${usuario.address.street}</td>
                    <td>${usuario.address.suite}</td>
                    <td>${usuario.address.city}</td>
                </tr>`;
            })
            .catch(error => {
                console.error("Error en la petición:", error);
            });
    }
});
