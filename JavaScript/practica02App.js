llamandoFetch= () =>{
    const tempUrl= "https://restcountries.com/v3.1/name/"
    let pais = document.getElementById('txtPais').value
    const url = tempUrl + pais
    fetch(url)
    .then(respuesta => respuesta.json())
    .then(data => mostrarDatos(data))
    .catch((reject)=>{
        console.log("surgio un error"+ reject)
    });
}

const mostrarDatos=(data)=>{
    console.log(data)
    const resCapital = document.getElementById('txtCapital')
    const resLenguaje = document.getElementById('txtLenguaje')
    resCapital.innerHTML="";
    resLenguaje.innerHTML="";


    for(let item of data){
        native = item.nativeName
        resCapital.innerHTML= item.capital
        const lenguaje = Object.values(item.languages)
        resLenguaje.innerHTML= lenguaje
    }
}

document.getElementById('btnBuscar').addEventListener('click', function(){
    let pais= document.getElementById('txtPais').value
    if(!pais){
        alert("Por favor, ingrese un pais.");
    }
    else{
        llamandoFetch();
    }
    
})

document.getElementById('btnLimpiar').addEventListener('click', function(){
    const resCapital = document.getElementById('txtCapital')
    const resLenguaje = document.getElementById('txtLenguaje')

    resCapital.innerHTML=""
    resLenguaje.innerHTML=""
})